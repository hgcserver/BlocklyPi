from distutils.core import setup
setup(
    name='ssd1306',
    version='1.0',
    description='ssd1306 library',
    author='hgcserver',
    author_email='594352301@qq.com',
    url='https://gitee.com/hgcserver',
    py_modules=['ssd1306']
)